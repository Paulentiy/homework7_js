let arr = ['hello', 'world', 'Kiev', 'Kharkiv', 'Odessa', 'Lviv'];

function createList(arr) {
  
   let newArr = arr.map(item => `<li>${item}</li>`);
  
   let outputToPage = newArr.join('');

   document.body.innerHTML = `<ul>${outputToPage}<ul>`;
  }

createList(arr);
